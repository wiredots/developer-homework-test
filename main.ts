import { GetProductsForIngredient, GetRecipes } from "./supporting-files/data-access";
import { NutrientFact } from "./supporting-files/models";
import { GetCostPerBaseUnit, GetNutrientFactInBaseUnits } from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */
    // since the recipe data is an array of recipe
   const cheapestCostRecipes = recipeData.map(recipe => {
        let totalCost = 0; // for the total cost each ingredient
        const nutrients: {[key: string]: NutrientFact} = {}; // object holder of nutrients with quantityAmount.uomAmount

        recipe.lineItems.forEach(({ ingredient, unitOfMeasure }) => {
            const availableProducts = GetProductsForIngredient(ingredient);

            // given data: each available products has multiple suppliers
            // goal: get the cheapest supplier of each available product and get all the cheapest of the cheapest supplier list
            const productsWithCheapestSupplier = availableProducts.map(({ nutrientFacts, productName, supplierProducts }) => {
                const cheapestSupplierUnitCosting = supplierProducts.reduce((prev, current) => {
                    return GetCostPerBaseUnit(prev) < GetCostPerBaseUnit(current) ? prev: current
                });
                return {
                    productName,
                    nutrientFacts,
                    supplier: cheapestSupplierUnitCosting
                }
            });
            
            // find the cheapest supplier among all products
            const cheapestSupplier = productsWithCheapestSupplier.reduce((prev, current) => {
                return GetCostPerBaseUnit(prev.supplier) < GetCostPerBaseUnit(current.supplier) ? prev: current
            });
            totalCost += GetCostPerBaseUnit(cheapestSupplier.supplier) * unitOfMeasure.uomAmount;

            // we need to store and populate the nutrients based on the nutrients info a product
            cheapestSupplier.nutrientFacts.forEach((nutrientFact) => {
                const nutrientBaseUnit = GetNutrientFactInBaseUnits(nutrientFact);
                if (nutrients[nutrientBaseUnit.nutrientName]) {
                    nutrients[nutrientBaseUnit.nutrientName].quantityAmount.uomAmount = nutrients[nutrientBaseUnit.nutrientName].quantityAmount.uomAmount + nutrientBaseUnit.quantityAmount.uomAmount
                } else {
                    nutrients[nutrientBaseUnit.nutrientName] = nutrientBaseUnit
                }
            });
        });
        
        // this is just for sorting to make match of the test fixture
        // TODO: make it a helper function
        // NOTE: if the test fixture doesnt need sorting this can be remove
        const sortedNutrientKeys = Object.keys(nutrients).sort();
        const sortedNutrients = sortedNutrientKeys.reduce((acc, current) => {
            acc[current] = nutrients[current];
            return acc;
        }, {});
    
        return {
            recipeName: recipe.recipeName,
            cheapestCost: totalCost,
            nutrientsAtCheapestCost: sortedNutrients
        }
    });

    const firstCheapestRecipe = cheapestCostRecipes.shift(); // get the first found recipe
    if (firstCheapestRecipe) {
        recipeSummary[firstCheapestRecipe.recipeName] = {
            cheapestCost: firstCheapestRecipe.cheapestCost,
            nutrientsAtCheapestCost: firstCheapestRecipe.nutrientsAtCheapestCost
        };
    }
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
